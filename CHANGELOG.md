<a name="0.1.2"></a>
## [0.1.2](https://github.com/coveo/eslint-config-coveo/compare/v0.1.0...v0.1.2) (2016-08-17)



<a name="0.1.0"></a>
# [0.1.0](https://github.com/coveo/eslint-config-coveo/compare/v0.0.9...v0.1.0) (2016-08-16)


### Bug Fixes

* **travis:** fix UT ([98d58d3](https://github.com/coveo/eslint-config-coveo/commit/98d58d3))


### Features

* **rules:** enables no-var ([166aacb](https://github.com/coveo/eslint-config-coveo/commit/166aacb))
* **test:** enables more options on some rules ([466b48f](https://github.com/coveo/eslint-config-coveo/commit/466b48f))


### BREAKING CHANGES

* test: test were separated into 2 tests AFTER CHANGE: 2 tests



<a name="0.0.9"></a>
## [0.0.9](https://github.com/coveo/eslint-config-coveo/compare/v0.0.8...v0.0.9) (2016-06-22)



<a name="0.0.8"></a>
## [0.0.8](https://github.com/coveo/eslint-config-coveo/compare/v0.0.7...v0.0.8) (2016-06-21)



<a name="0.0.7"></a>
## [0.0.7](https://github.com/coveo/eslint-config-coveo/compare/v0.0.5...v0.0.7) (2016-06-20)



<a name="0.0.5"></a>
## [0.0.5](https://github.com/coveo/eslint-config-coveo/compare/v0.0.4...v0.0.5) (2016-06-07)



<a name="0.0.4"></a>
## [0.0.4](https://github.com/coveo/eslint-config-coveo/compare/v0.0.3...v0.0.4) (2016-06-07)



<a name="0.0.3"></a>
## [0.0.3](https://github.com/coveo/eslint-config-coveo/compare/v0.0.1...v0.0.3) (2016-06-07)



<a name="0.0.1"></a>
## 0.0.1 (2016-06-07)



